import sympy
import math

def Print_aantal_wortels(a,b,c):
    det = b**2-(4*a*c)
    if(det<0):
        return "Deze vierkantsvergelijking heeft geen reële wortels."
    if(det == 0):
        return "Deze vierkantsvergelijking heeft een reële wortel."
    if(det >0):
        return "Deze vierkantsvergelijking heeft twee reële wortels."

def Print_wortels(a,b,c):
    det = b**2-(4*a*c)
    if(det == 0):
        return "Deze vierkantsvergelijking heeft een reële wortel: ",((-b+(det**0.5))/(2*a))
    elif(det >0):
        return "Deze vierkantsvergelijking heeft twee reële wortels: ",((-b+(det**0.5))/(2*a)),((-b-(det**0.5))/(2*a))
    else:
        return "Deze vierkantsvergelijking heeft geen reële wortels."

def box(s):
    offset = 3
    c = len(s)
    print('+' + '-'*(c+offset*2) + '+')
    print('|' + ' '*offset + s + ' '*offset + '|')
    print('+' + '-'*(c+offset*2) + '+')

def getuserinput():
    print("Type een tekst:")
    s = input()
    box(s)

def schaakbord():
    prevresult = 1
    totaal = 1
    print ('field 1, value: ', prevresult)
    for i in range(63):
        prevresult = prevresult * 2
        totaal = totaal + prevresult
        print ('field ' , (i+2) , ', value: ', prevresult)
    print('totaal = ', totaal)
    

