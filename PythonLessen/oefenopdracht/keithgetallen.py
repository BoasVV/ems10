
def is_keithgetal (n):

    if(n < 9): # start waarde mag niet kleiner zijn als 9
        return False
    getal_als_string = str(n)

    result_list = []
    for getal in getal_als_string:
        result_list.append(int(getal)) # stop de eerste waardes van de reeks in een lijst

    pos = 0 # start positie in de lijst
    while(True): # blijft altijd doorlopen
        new_digit = 0 # het nieuwe getal voor in de lijst

        for i in range(len(getal_als_string)):
            new_digit = new_digit + result_list[pos+i] # tel de voorgaande getallen (aantal ligt vast aan de hoeveelheid charakters in de start waarde) bij elkaar op

        pos = pos + 1 # schuif een positie op in de lijst

        result_list.append(new_digit) # het nieuwe getal toevoegen aan de lijst

        # controleer of de waardes voldoen aan de eisen om voor te komen in de keith getallen reeks
        if(new_digit == n):
            return True # als het nieuwe getal even groot is als het gegeven getal dan is het een keith getal
        elif(new_digit > n):
            return False # het nieuwe getal is groter als het getal dat benodid is het is dus geen keith getal
        else:
            pass # doe niks omdat het getal nog kleiner is als het benodigde antwoord

def Test_is_keithgetal():
    for i in range(10,10**4):
        if(is_keithgetal(i)):
            print(i)
        else:
            pass

#Test_is_keithgetal()
# print(is_keithgetal(int(input("een getal hier: "))))

'''
kwadraat in 2 getallen
evenveel cijfers als het orgineel
opgeteld zelfde getal als orgineel
LET OP getallen 4879 en 5292 komen niet uit deze berekening omdat complexere berekeningen nodig hebben
'''
def is_KaprekarGetallen(getal):
    pow = getal**2  # eerste berekening
    str_pow = str(pow) # maak string van het berekende getal

    if(pow < 10): # als de uitkomst van de berekening kleiner is als 10 kan er niet gesplitst worden dus is het 0 + n
        str_value1 = "0"
        str_value2 = str_pow
    else:
        str_value1 = str_pow[:(len(str_pow)//2)] # splits het antwoord in 2 getallen
        str_value2 = str_pow[(len(str_pow)//2):]

    result = int(str_value1) + int(str_value2) # bereken de uitkomst van de optelling

    if(result == getal): # als de uitkomst gelijk is aan het begin getal dan is het een kaprekar getal
        return True
    else:
        return False

def Test_is_KaprekarGetallen():
    for i in range(1,10**4):
        if(is_KaprekarGetallen(i)):
            print(i)
        else:
            pass

# Test_is_KaprekarGetallen()

def constante_van_karprekar(getal):
    result = getal # bewaar het orgineel (voor de rest niet nodig)
    count = 0 # houd bij hoevaak er rond wordt gelopen door de loop
    while True: # blijf doorlopen
        str_getal = str(result) # maak het getal een string voor sorteren
        rtl = ''.join(sorted(str_getal,reverse=True)) # sorteer aflopend  ( join voegt de waardes uit een lijst samen bijv. [1,2,3] wordt '123')
        ltr = ''.join(sorted(str_getal)) # sorteer oplopend

        if(int(rtl) < int(ltr)): # grote getal laten aftrekken van kleine getal
            result = int(ltr) - int(rtl)
        else:
            result = int(rtl) - int(ltr)

        if(result == 6174): # als waarde bereikt dan stoppen
            break
        elif(result == 0): # als waarde gelijk is aan nul stopen
            count = "Error"
            break # stop de loop

        count = count + 1 # tel loops

    print("het antwoord is berijkt in %s loops"%(count))

constante_van_karprekar(int(input()))

input("press enter to exit")
