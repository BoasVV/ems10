import turtle
import math

def square(t,length):
    for i in range(4):
        t.fd(length)
        t.lt(90)

def polygon(t,n,length):
    angle = 360/n
    for i in range(n):
        t.fd(length)
        t.lt(angle)

def circle(t, r):
    circumference = 2 * math.pi * r
    n = int(circumference / 3) + 3
    length = circumference / n
    polygon(t, n, length)

def arc(t, r, angle):
    arc_length = 2 * math.pi * r * angle / 360
    n = int(arc_length / 3) + 1
    step_length = arc_length / n
    step_angle = angle / n
    
    for i in range(n):
        t.fd(step_length)
        t.lt(step_angle)

bob = turtle.Turtle()
polygon(bob, n=5, length=100)
bob.fd(10)
for i in range(4):
    arc(bob,r=40,angle=90)
    bob.fd(120)
bob.fd(40)
bob.lt(90)
square(bob,200)
bob.fd(100)
bob.circle(100)
turtle.mainloop()