'''import numpy as np
import matplotlib.pyplot as plt

x1 = np.linspace(0,4*np.pi,200)
x2 = np.linspace(0,4*np.pi, 50)
y1 = np.sin(x1)
y2 = np.cos(x2)
plt.title('grafiek')
plot1 = plt.plot(x1,y1)
plot2 = plt.plot(x2,y2)
plt.xlabel('x-as')
plt.ylabel('y-as')
#legend


plt.show()'''


import numpy as np
import matplotlib.pyplot as plt

def H1(w):
    return 1j*w*22e-3 / (1e2 + 1j*w*22e-3)

def H2(w):
    return (1/(1j*w*1e-6) + 1j*w*22e-3) / (100 + 1/(1j*w*1e-6) + 1j*w*22e-3)

def plot_bodediagram(h):
    plt.subplot(211)
    plt.grid(True)
    
    f = np.logspace(1,5)
    w = 2*np.pi*f
    plt.semilogx(f,h(w))

    plt.subplot(212)
    plt.grid(True)
    
    datapoints = np.angle(h(w))
    plt.semilogx(f,datapoints)
    
plt.figure(1)
plot_bodediagram(H1)
plt.figure(2)
plot_bodediagram(H2)
plt.show()
