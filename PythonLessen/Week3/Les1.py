'''import math

test_list= [3.2,5.7,8.5,9.1]

def avg(list):
    lengte = len(list)
    result = 0
    for items in list:
        result = result + items
    return result/lengte

def std_dev(list):
    u = avg(list)
    result = 0
    for i in list:
        result = (result + ((i - u)**2))
    return ((result/len(list))**0.5)

print(std_dev(test_list))'''


toetsresultaten = [1.2 , 3.6 , 9.2 , 8.2 , 3.5 , 5.5 , 6.0 , 7.4,5.9 , 6.0 , 5.4 , 5.2 , 4.3 , 7.4 , 1.2 , 1.0]

def rendement_toets(lijst):
    count = 0
    for i in lijst:
        if(i>=5.5):
            count += 1
    rendement = (count/len(lijst))*100

    if(rendement < 51):
        return rendement_toets( pas_toetsresultaten_aan(lijst) )
    return rendement

def pas_toetsresultaten_aan(lijst):
    pos = 0;
    for i in lijst:
        if (i < 10):
            lijst[pos] = i + 0.1
        pos += 1
    return lijst

print(rendement_toets(toetsresultaten),'%')
