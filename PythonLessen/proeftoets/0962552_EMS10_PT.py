def som_van_de_cijfers(getal):
    result = 0                              # Define start waarde
    for g in str(getal):                    # Zet getal om in string zodat waardes gesplitst kunnen worden loop daarna een voor een door de characters heen
        result += int(g)                    # Converteer de string weer naar een int en tel hem op bij result
    return result                           # Geef result terug

def is_Harshadgetal(getal):
    svdc = som_van_de_cijfers(getal)        # Krijg de som van de cijfers 
    rest = getal % svdc                     # Bereken de restwaarde
    if(rest == 0):                          # Als de restwaarde gelijk is aan 0 dan heb je een Harshadgetal
        return True                         # Return dat het een Harshadgetal is
    else:
        return False                        # Return dat het geen Harshadgetal is

def Test_is_Harshadgetal():                 # Test functie
    for getal in range(81,110):             # Loop door de getallen binnen de gestelde reeks heen
        if(is_Harshadgetal(getal)):         # Controleer of het een Harshadgetal is
            print(getal)                    # Print het Harshadgetal
            
#print(som_van_de_cijfers(132))             # Antwoord = 6
print(is_Harshadgetal(132))                # Antwoord = True
#Test_is_Harshadgetal()                      # Antwoorden zijn 81,84,90,100,102,108

