from math import log10,floor
from enum import Enum

class SI(Enum):
    m = -3
    o = 0
    K = 3
    M = 6
    G = 9

def calc_E12_base(n):
    if (n < 1) or (n>12):
        return -1
    return round(10*10**((n-1)/12))

def TestE12_base():
    for i in range(0,14):
        print("E12_base(%s) = "%(i), E12_base(i))

def E12_base(n):
    value = calc_E12_base(n)
    if(n>5) and (n<10):
        value += 1
    if(n == 12):
        value -= 1
    return value

def E12_value(n,p):
    value = E12_base(n)
    print (value)
    if(value == -1 or (p < -3 and p > 9)):
        return -1
    return value*10**p

def print_E12_value(value):
    si = floor(log10(value))
    for i in range(si-2, si):
        try:
            SI(i)
            si = i
            break
        except:
            pass
    print ("%.2f"%(value/10**si), SI(si).name if SI(si).name != SI.o.name else '', sep=' ',end=u"\u2126  ")

def print_E12_range():
    for mult in range(-4,11):
        for value in range(1,13):
            print_E12_value(E12_value(value,mult))

class Resistor():
    Ring1 = None
    Ring2 = None
    Ring3 = None
    
    def resistorCompleted(self):
        if(self.Ring1 == None or self.Ring2 == None or self.Ring3 == None):
            return False
        else:
            return True

    class ColorCodes(Enum):
        PK = -3
        SR = -2
        GD = -1
        BK = 0
        BN = 1
        RD = 2
        OG = 3
        YE = 4
        GN = 5
        BU = 6
        VT = 7
        GY = 8
        WH = 9
    
def inputColorCode():
    res = Resistor()
    print("Enter colorCodes")
    while(True):
        while(not res.resistorCompleted()):
            try:
                if(res.Ring1 == None):
                    res.Ring1 = res.ColorCodes[input("Ring 1 = ")]
                elif(res.Ring2 == None):
                    res.Ring2 = res.ColorCodes[input("Ring 2 = ")]
                    err = True
                    for i in range(0,14):
                        calc = E12_base(i)
                        if(calc == ((res.Ring1.value*10)+res.Ring2.value)):
                            err = False
                    if(err == True):
                        print("value %s does not exists in the E12 range, Try again:"%(print_E12_value((res.Ring1.value*10)+res.Ring2.value)))
                        res = Resistor()
                elif(res.Ring3 == None):
                    res.Ring3 = res.ColorCodes[input("Ring 3 = ")]
            except:
                print("Please try again with a valid color code")
                print("Valid Color codes are:")
                for code in res.ColorCodes:
                    print("%s = %s"%(code.name,code.value))
                    
        print("Ring 1 = %s, Ring 2 = %s, Ring 3 = %s"%(res.Ring1.value,res.Ring2.value,res.Ring3.value))

        print("The value of this resistor is: ")
        print_E12_value(((res.Ring1.value*10)+res.Ring2.value)*10**res.Ring3.value)
        print()

        while(True):
            response = input("Do you want to enter a new value? [Y/N]: ")
            if(response == 'N' or response == 'n'):
                exit()
            elif(response == 'Y' or response == 'y'):
                res = Resistor()
                break
            else:
                print("Please use Y or N")

inputColorCode()
