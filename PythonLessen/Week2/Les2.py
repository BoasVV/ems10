def ggd(a,b):
    try:
        if(a == b):
            return a
        if(a>b):
            return ggd(a-b,b)
        else:
            return ggd(a,b-a)
    except(ValueError):
        print("Invalid value")
    except(RecursionError):
        print("this value can not be calculated")
    except(TypeError):
        print("One of the given values is not suported")
        

def print_hms(s):
    seconds = s % 60
    minuts = (s//60)%60
    hours = (s//60)//60
    print(hours,minuts,seconds,sep=':')

def factorial(n):
    if (n==0):
        return 1
    else:
        recurse = factorial(n-1)
        result = n*recurse
        return result

def Fact(n):
    result = 1
    for i in range(n):
        result = result *(i+1)
    return result

def FactWhile(n):
    mult = 0
    result = 1
    while(n != mult):
        mult=mult+1
        result = result * mult
    return result

def my_sqrt(x,epsilon):
    a=x
    while True:
        y = (x+a/x)/2
        if(abs(y-x)<epsilon):
           return y
        x=y

def calc_E12_base(n): # incorrect formule wijkt af
    if(n >=1 and  n<=12):
        return round(10*10**((n-1)/12))
    else:
        return -1
